package net.j4love.mybatis.plus.common;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * @author He Peng
 * @create 2017-07-02 14:25
 * @update 2017-07-02 14:25
 * @updatedesc : 更新说明
 * @see
 */
public abstract class AbstractSimpleDataAccessService<T>
        implements SimpleDataAccessable<T> {

    @Inject
    private BaseMapper<T> baseDAO;

    @Override
    public Integer addOne(T entity) {
        return this.baseDAO.insert(entity);
    }

    @Override
    public Integer addBatch(List<T> entitys) {
        return null;
    }

    @Override
    public Integer updateSelectiveById(T entity) {
        return this.baseDAO.updateById(entity);
    }

    @Override
    public Integer deleteById(Serializable id) {
        return this.baseDAO.deleteById(id);
    }

    @Override
    public T getOneById(Serializable id) {
        return this.baseDAO.selectById(id);
    }

    @Override
    public List<T> listByCondition(T entity) {
        Wrapper<T> wrapper = new EntityWrapper<>(entity);
        return this.baseDAO.selectList(wrapper);
    }
}
