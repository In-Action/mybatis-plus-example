package net.j4love.mybatis.plus.common;

import java.io.Serializable;
import java.util.List;

/**
 * @author He Peng
 * @create 2017-07-02 14:11
 * @update 2017-07-02 14:11
 * @updatedesc : 更新说明
 * @see
 */
public interface SimpleDataAccessable<T> {

    Integer addOne(T entity);

    Integer addBatch(List<T> entitys);

    Integer updateSelectiveById(T entity);

    Integer deleteById(Serializable id);

    T getOneById(Serializable id);

    List<T> listByCondition(T entity);
}
