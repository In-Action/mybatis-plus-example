package net.j4love.mybatis.plus.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import net.j4love.mybatis.plus.model.User;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.ResultHandler;

import java.util.List;
import java.util.Map;

/**
 * @author He Peng
 * @create 2017-07-01 23:24
 * @update 2017-07-01 23:24
 * @updatedesc : 更新说明
 * @see
 */
public interface UserDAO extends BaseMapper<User> {

    @ResultType(Map.class)
    @Select("select * from `user`")
    void selectAll(ResultHandler<Map<String , Object>> resultHandler);
}
