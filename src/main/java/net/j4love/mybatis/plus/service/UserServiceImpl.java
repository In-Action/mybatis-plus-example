package net.j4love.mybatis.plus.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import net.j4love.mybatis.plus.common.AbstractSimpleDataAccessService;
import net.j4love.mybatis.plus.dao.UserDAO;
import net.j4love.mybatis.plus.model.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * @author He Peng
 * @create 2017-07-02 14:32
 * @update 2017-07-02 14:32
 * @updatedesc : 更新说明
 * @see
 */

@Service
public class UserServiceImpl
        extends ServiceImpl<UserDAO,User>
        implements UserService {

    @Inject
    private UserDAO userDAO;
}
