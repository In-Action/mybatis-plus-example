package net.j4love.mybatis.plus.service;

import com.baomidou.mybatisplus.service.IService;
import net.j4love.mybatis.plus.common.SimpleDataAccessable;
import net.j4love.mybatis.plus.model.User;

/**
 * @author He Peng
 * @create 2017-07-02 14:24
 * @update 2017-07-02 14:24
 * @updatedesc : 更新说明
 * @see
 */
public interface UserService extends IService<User> {
}
