package net.j4love.mybatis.plus.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

/**
 * @author He Peng
 * @create 2017-06-22 23:31
 * @update 2017-06-22 23:31
 * @updatedesc : 更新说明
 * @see
 */

@TableName(
        value = "user",
        resultMap = "net.j4love.mybatis.plus.dao.UserDAO.BasicResultMap"
)
public class User {

    @TableId(
            value = "id",
            type = IdType.AUTO
    )
    private Long id;

    private String nickname;
    private String realname;
    private Long phone;

    /*@TableField(
            value = "login_password"
    )*/
    private String loginPassword;

    /*@TableField(
            value = "pay_password"
    )*/
    private String payPassword;

    /*@TableField(
            value = "create_time"
    )*/
    private Date createTime;

    /*@TableField(
            value = "update_time"
    )*/
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", realname='" + realname + '\'' +
                ", phone=" + phone +
                ", loginPassword='" + loginPassword + '\'' +
                ", payPassword='" + payPassword + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
