package net.j4love.mybatis.plus.dao;

import net.j4love.mybatis.plus.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import java.util.Date;
import java.util.Map;

/**
 * @author He Peng
 * @create 2017-07-01 23:37
 * @update 2017-07-01 23:37
 * @updatedesc : 更新说明
 * @see
 */

/**
 *
 * @author He Peng
 * @create 2017-07-01 23:37
 * @update 2017-07-01 23:37
 * @updateDesc : 更新说明
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        locations = "classpath:spring/applicationContext-dao.xml"
)
public class UserDAOTest {

    @Inject
    private UserDAO userDAO;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void insert() throws Exception {
        User user = new User();
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("jgl");
        user.setRealname("绿灯侠");
        user.setPhone(13320963328L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");

        Integer count = this.userDAO.insert(user);
        Assert.assertEquals(Integer.valueOf(1),count);
    }

    @Test
    public void selectAll() {
        userDAO.selectAll(new ResultHandler<Map<String, Object>>() {
            @Override
            public void handleResult(ResultContext<? extends Map<String, Object>> resultContext) {
                System.out.println(resultContext.getResultObject());
            }
        });
    }

    @Test
    public void selectOne() throws Exception {
        User dbUser = this.userDAO.selectById(7L);
        System.out.println(dbUser);
        Assert.assertNotNull(dbUser);
    }

    @Test
    public void selectPage() throws Exception {


    }
}