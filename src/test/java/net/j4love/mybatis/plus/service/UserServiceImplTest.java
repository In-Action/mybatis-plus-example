package net.j4love.mybatis.plus.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import net.j4love.mybatis.plus.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static org.junit.Assert.*;
/**
 * @author He Peng
 * @create 2017-07-02 14:34
 * @update 2017-07-02 14:34
 * @updatedesc : 更新说明
 * @see
 */

/**
 *
 * @author He Peng
 * @create 2017-07-02 14:34
 * @update 2017-07-02 14:34
 * @updateDesc : 更新说明
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        locations = "classpath:spring/applicationContext-dao.xml"
)
public class UserServiceImplTest {

    @Inject
    private UserService userService;
    
    @Test
    public void getOneById() throws Exception {

        User dbUser = this.userService.selectById(1);
        System.err.println(dbUser);
        Assert.assertNotNull(dbUser);
    }

    @Test
    public void selectPage() throws Exception {

        Page<User> page = new Page<>(1,3);
        Wrapper<User> wrapper = new EntityWrapper<>();
        wrapper.ge("id",3);
        Page<User> resultPage = this.userService.selectPage(page, wrapper);
        System.err.println(resultPage);
    }
}